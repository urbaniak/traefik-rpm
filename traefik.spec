%define debug_package %{nil}

Name:           traefik
Version:        1.3.2
Release:        0%{?dist}
Summary:        Træfɪk, a modern reverse proxy
ExclusiveArch:  x86_64

Group:          System Environment/Daemons
License:        MIT
URL:            https://traefik.io/
Source0:        https://github.com/containous/traefik/releases/download/v%{version}/traefik_linux-amd64
Source1:        traefik.service
Source3:        https://raw.githubusercontent.com/containous/traefik/master/traefik.sample.toml
Source4:        LICENSE

BuildRequires:  systemd-units

Requires(pre):  shadow-utils
Requires:       systemd glibc

%description
Træfɪk is a modern HTTP reverse proxy and load balancer made to deploy
microservices with ease. It supports several backends (Docker, Swarm,
Mesos/Marathon, Consul, Etcd, Zookeeper, BoltDB, Rest API, file...) to manage
its configuration automatically and dynamically.

%prep

%build

%install
install -D %{SOURCE0} %{buildroot}/%{_bindir}/traefik
install -D %{SOURCE1} %{buildroot}/%{_unitdir}/%{name}.service
install -D %{SOURCE3} %{buildroot}/%{_sysconfdir}/%{name}/traefik.toml
install -D %{SOURCE4} %{buildroot}/%{_docdir}/%{name}/LICENSE

%pre
getent group %{name} >/dev/null || groupadd -r %{name}
getent passwd %{name} >/dev/null || \
    useradd -r -g %{name} -d %{_sharedstatedir}/%{name} -s /sbin/nologin \
    -c "%{name} user" %{name}
exit 0

%post
%systemd_post %{name}.service

%preun
%systemd_preun %{name}.service

%postun
getent passwd %{name} >/dev/null && userdel %{name}
getent group %{name} >/dev/null && groupdel %{name}
%systemd_postun_with_restart %{name}.service

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root,-)
%attr(755, root, root) %{_bindir}/traefik
%dir %attr(750, root, %{name}) %{_sysconfdir}/%{name}
%attr(644, root, root) %{_unitdir}/%{name}.service
%config(noreplace) %attr(640, root, %{name}) %{_sysconfdir}/%{name}/traefik.toml
%doc %{_docdir}/%{name}/LICENSE
